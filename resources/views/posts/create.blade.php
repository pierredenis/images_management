<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{URL::asset('css/image-picker.css')}}">
    <title>Document</title>
    <style>
        .image_picker_image{
            width: 100px;
            height: 100px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10">
                <br>
                <h1 class="text-center">JE SOUHAITE CRÉER UN ARTICLE </h1>
                {!! Form::open(['route' => 'posts.store','class'=>'col-12']) !!}
                <div class="form-group">
                    <label>Un titre !</label>
                    {{Form::text('name',null,['class'=>'form-control'])}}
                </div>
                <br>
                <div class="form-group">
                    <label>Un Contenu !</label>
                    {{Form::textarea('content',null,['class'=>'form-control'])}}
                </div>
                <br>
                <div class="form-group">
                    <label>Une image existante ! </label>
                    <select name="image_id" id="">
                        @foreach($images as $image)
                            <option style="width:100px;height:100px;" data-img-src="{{Storage::url($image->url.'/'.$image->name)}}" value="{{$image->id}}">{{$image->name}}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                <div class="form-group">
                    {{Form::submit('ENVOYER !!!!',['class' => 'btn btn-primary btn-lg text-center col-4'])}}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</body>
<script
    src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
    crossorigin="anonymous"></script>
<script src="{{URL::asset('js/image-picker.min.js')}}"></script>
<script>$("select").imagepicker()</script>
</html>
