<html lang="en" class="">
<head>

    <meta charset="UTF-8">
    <title>Laravel Image Upload Tutorial Example From Scratch</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <style>
        img{
            margin: 10px 0px;
        }
    </style>
</head>
<body>
<div class="container">
    <br><br><br>
    @if ($message = Session::get('success'))

        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        <br>
    @endif
    <h2>File Upload &amp; Image Preview</h2>
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif
    <!-- Upload  -->
    {!! Form::open(['route'=>'images.store','files'=>true]) !!}
    {{Form::file('fileUpload')}}
    {{Form::submit()}}
    {!! Form::close() !!}
    @if($images)
    <div class="col-12 row justify-content-center">
        @foreach($images as $image)
            <img class="col-3" src="{{Storage::url($image->url.'/'.$image->name)}}"" alt="">
        @endforeach
    </div>
    @endif
</body>
