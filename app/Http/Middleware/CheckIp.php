<?php

namespace App\Http\Middleware;

use Closure;

class CheckIp
{
    private $whitelist = [
        '127.0.0.1',
        '12.41.24.124'
    ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(in_array($request->ip(),$this->whitelist)){
            return $next($request);
        }
        return abort('403');


    }
}
