<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageRequest;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public function store(ImageRequest $request){

        $myFile = $request->fileUpload;

        $actualName = $myFile->getClientOriginalName();
        $path = 'images/'.$this->getYearMonth();

        if(Storage::disk('public')->putFileAs($path,$myFile,$actualName)) {
            $image = Image::create(['name' => $actualName, 'url' => $path]);
        }
    }

    public function create(){
        $images = Image::all();
        return view('images.create',['images' => $images]);
    }

    /**
     * return a string of year/date.
     *
     * @return String
     */
    private function getYearMonth(){
        $date = Carbon::now();
        return $date->year.'/'.$date->month;
    }
}
