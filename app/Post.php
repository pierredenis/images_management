<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['name','content','image_id'];
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function image(){
        return $this->belongsTo(Image::class);
    }
}
