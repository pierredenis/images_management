<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['name','url'];

    public function posts(){
        return $this->hasMany(Post::class);
    }
}
