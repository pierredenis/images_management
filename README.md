<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

# Simple Image Management System with laravel 

## About project 

This is a very simple project to help my students to understand saving files in Storage folder. 

If you want to help, fork and request ! 

## Getting Started

- Clone project 
- Create a database on your server and create a .env file
- launch : ``` composer install ```
- launch : ``` npm install ```
- to execute migration and seeding execute : ``` php artisan migrate --seed ``` 
- launch a local server with : ``` php artisan serve ``` 
- if you want to see available routes ``` php artisan route:list ```


## License

All this work, including the Laravel framework  is open-sources and licensed under the [MIT license](https://opensource.org/licenses/MIT).
