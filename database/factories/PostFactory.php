<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'name' => $faker->realText($faker->numberBetween($min = 15, $max = 50)),
        'content' => $faker->realText(100)
    ];
});
