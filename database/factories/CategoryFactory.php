<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->realText($faker->numberBetween($min = 15, $max = 30)),
        'description' => $faker->text(10)
    ];
});
