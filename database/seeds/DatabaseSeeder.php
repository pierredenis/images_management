<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Category::class, 5)->create()->each(function (App\Category $category){
           $category->posts()->saveMany(Factory(App\Post::class,10)->make());
        });

    }
}
